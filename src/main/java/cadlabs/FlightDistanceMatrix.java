package cadlabs;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.mllib.linalg.SparseVector;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import scala.Tuple2;

public class FlightDistanceMatrix {

	private static final String DefaulftFile = "data/flights.small.csv";


	public static void main(String[] args) {
		
		String file = (args.length < 1) ? DefaulftFile : args[0];
		
		// start Spark session (SparkContext API may also be used) 
		// master("local") indicates local execution
		SparkSession spark = SparkSession.builder().
				appName("FlightAnalyser").
				master("local[*]").
				getOrCreate();
		
		// only error messages are logged from this point onward
		// comment (or change configuration) if you want the entire log
		spark.sparkContext().setLogLevel("ERROR");

		
		Dataset<String> textFile = spark.read().textFile(file).as(Encoders.STRING());	
		
		/*
		 * Dataset with all flight info
		 */
		Dataset<Row> flights = 
				textFile.map((MapFunction<String, Row>) l -> Flight.parseFlight(l), 
				Flight.encoder()).cache();
		
		
		/*
		 * Dataset with (orig, dest, avg(distance)) 
		 * using internal ids to have a continuous id domain starting in 0
		 */
		Dataset<Row> flightDistance = 
					flights.select("origInternalId", "destInternalId", "distance")
							.groupBy("origInternalId", "destInternalId").agg(functions.avg("distance"));
		Dataset<Row>distFlight= flights.select("destInternalId","origInternalId","distance")
				.groupBy("destInternalId","origInternalId").agg(functions.avg("distance"));
		
		final int numberAirports = (int) flightDistance.count();
		/*
		 * Creating the adjacent matrix 
		 */
		JavaRDD<MatrixEntry> entries = flightDistance.toJavaRDD().map(f -> 
			new MatrixEntry(f.getLong(0), f.getLong(1), f.getDouble(2)));
		
		JavaPairRDD<Long, Vector> matrix = new CoordinateMatrix(entries.rdd()).transpose().
				toIndexedRowMatrix().rows().toJavaRDD().mapToPair(r -> new Tuple2<Long, Vector>(r.index(), r.vector()));
			
		/*
		 *  Creating the d vector
		 */
		List<Tuple2<Long, Double>> l = new ArrayList<Tuple2<Long, Double>>(numberAirports);
		for (int i = 0; i < numberAirports; i++)
			l.add(i, new Tuple2<Long, Double>((long)i, Double.MAX_VALUE));
		
		@SuppressWarnings("resource")
		JavaPairRDD<Long, Double> d =  new JavaSparkContext(spark.sparkContext()).parallelize(l).mapToPair(r -> r);
		
		
		System.out.println("Number of airports " +  numberAirports);
	
		Random rand = new Random();
		/*
		 * Joining the vector and the matrix 
		 */
		for (int i = 0; i < numberAirports; i++) {
			
			int node = rand.nextInt((int) numberAirports);
			
			System.out.println("------------------- iteration " +  i + ": process node " + node);	
			JavaPairRDD<Long, Tuple2<Double, Vector>> dmatrix = d.join(matrix);
		
		
			
			d = dmatrix.mapToPair(e -> new Tuple2<Long, Double> (e._1, updateD(node, e._2)));
		
			for (Tuple2<Long, Double> e : d.collect())
				System.out.println(e);	
		}
		
		// terminate the session
		spark.stop();
	}
	

	private static Double updateD(int node, Tuple2<Double, Vector> entry) {
		
		SparseVector sv  = entry._2.toSparse();
		double value = sv.apply(node);
		
		if (value == 0) // node entry for node
			return entry._1;
		
		return value;
	}
}
