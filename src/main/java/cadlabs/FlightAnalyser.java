package cadlabs;
import java.util.Timer;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.mllib.linalg.SparseVector;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import scala.Tuple2;
import scala.Tuple3;

public class FlightAnalyser {

	private static final String DefaulftFile = "data/flights.csv";


	public static void main(String[] args) throws FileNotFoundException {
		 long startTime = System.currentTimeMillis();
		String file = (args.length < 1) ? DefaulftFile : args[0];
		//PrintWriter out = new PrintWriter("filename.txt");
		// start Spark session (SparkContext API may also be used) 
		// master("local") indicates local execution
		SparkSession spark = SparkSession.builder().
				appName("FlightAnalyser").
				master("local[*]").
				getOrCreate();

		// only error messages are logged from this point onward
		// comment (or change configuration) if you want the entire log
		spark.sparkContext().setLogLevel("ERROR");

		Dataset<String> textFile = spark.read().textFile(file).as(Encoders.STRING());	
		Dataset<Row> flights = 
				textFile.map((MapFunction<String, Row>) l -> Flight.parseFlight(l), 
						Flight.encoder()).cache();


		float reductionFactor = 0.01f;
		try {
			reductionFactor = args.length < 2 ? reductionFactor : Float.parseFloat(args[1]);
			if(reductionFactor > 1 || reductionFactor < 0)
				System.out.println("The reduction factor should be in the following range ]0, 1[.");
			else {
				//TODO: Our Program
				float auxR = reductionFactor;
				System.out.println("Begin our program with a reduction factor of: " + auxR + ".");

				Dataset<Row> flightsDest = flights.as("flightsDest").select(
						flights.col("destInternalId").as("origInternalId"),
						flights.col("origInternalId").as("destInternalId"),
						flights.col("departure delay"));

				Dataset<Row> flightsOrig = flights.as("flightsOrig").select(
						flights.col("origInternalId"),
						flights.col("destInternalId"),
						flights.col("departure delay"));

				Dataset<Row> aux = flightsOrig.union(flightsDest);
				Dataset<Row> delay = aux.groupBy(aux.col("origInternalId"), aux.col("destInternalId"))
						.agg(functions.avg(aux.col("departure delay"))).cache();
				int numberOfAirports=(int) delay.select(delay.col("origInternalId")).distinct().count();
				for(Row i: delay.collectAsList())
					System.out.println(i);
				System.out.println("Number of Airports: " + numberOfAirports);

				JavaRDD<MatrixEntry> undirectGraph = delay.toJavaRDD().map(f -> 
				new MatrixEntry(f.getLong(0), f.getLong(1), f.getDouble(2) == 0.0 ? 0.001 : f.getDouble(2)));
		
				JavaPairRDD<Long, Vector> matrix = new CoordinateMatrix(undirectGraph.rdd()).transpose().
						toIndexedRowMatrix().rows().toJavaRDD().mapToPair(r -> new Tuple2<Long, Vector>(r.index(), r.vector())).cache();

				JavaRDD<Tuple3<Long, Long, Double>> d = prim(matrix, numberOfAirports, spark).values().cache();
				Double minWeight = d.reduce((x, y) -> new Tuple3<Long, Long, Double>(x._1(), x._2(), x._3() + y._3()))._3();
				System.out.println("Minimum total edge: " + minWeight);
				System.out.println("Minimum Spaning Tree:");
				//y=d.groupBy(f->f._1 ,f._2);
				//Dataset<Row>dsD= spark.createDataFrame(d, beanClass)
				for(Tuple3<Long, Long, Double> r: d.collect())
					System.out.println(r);
				
				JavaRDD<Tuple3<Long, Long, Double>> duplicated = d.union(d.map(x -> new Tuple3<Long, Long, Double>(x._2(), x._1(), x._3())));
				//System.out.println("Duplicated:");
				//for(Tuple3<Long, Long, Double> r: duplicated.collect())
				//	System.out.println(r);
				
				JavaRDD<Tuple3<Long, Long, Double>> allAirports = delay.toJavaRDD().map(f->new Tuple3<Long, Long, Double>(f.getLong(0), f.getLong(1), f.getDouble(2)));
				
				JavaRDD<Tuple3<Long, Long, Double>> complement = allAirports.subtract(duplicated);
				JavaRDD<Tuple3<Long, Long, Double>> compOfComp = allAirports.subtract(complement);
				//System.out.println("Complement:");
				//for(Tuple3<Long, Long, Double> r: complement.collect())
				//	System.out.println(r);
				Tuple2<Long, Tuple3<Long, Long, Double>> bottleneck = complement.mapToPair(e -> new Tuple2<Long, Tuple3<Long, Long, Double>> (e._1(), e))
						.reduceByKey((x, y) -> new Tuple3<Long, Long, Double>(x._1(), x._2(), x._3()+y._3()))
						.reduce((x,y) -> x._2._3() > y._2._3() ? x : y);
			
				System.out.println("Bottleneck Airport " + bottleneck._2._1() + " with aggregated delay time of: " + bottleneck._2._3() );
				
				JavaRDD<MatrixEntry> newGraphAux = complement.map(f -> createNewGraph(f, bottleneck._2._1(), auxR));
				
				JavaRDD<MatrixEntry> newGraph = newGraphAux.union(compOfComp.map(f -> new MatrixEntry(f._1(), f._2(), f._3())));
				
				
				JavaPairRDD<Long, Vector> matrix2 = new CoordinateMatrix(newGraph.rdd()).transpose().
						toIndexedRowMatrix().rows().toJavaRDD().mapToPair(r -> new Tuple2<Long, Vector>(r.index(), r.vector())).cache();
				
				System.out.println("New values for all airports:");
				for(MatrixEntry r: newGraph.collect())
					System.out.println(r);
				
				JavaRDD<Tuple3<Long, Long, Double>> newD = prim(matrix2, numberOfAirports, spark).values();
				System.out.println("New MST:");
				for(Tuple3<Long, Long, Double> r: newD.collect())
					System.out.println(r);
				long stopTime = System.currentTimeMillis();
			      long elapsedTime = stopTime - startTime;
			      System.out.println(elapsedTime);
				
			}
		} catch (NumberFormatException nfe) {
			System.out.println("The argument for the reduction factor should be a number between ]0, 1[.");
		}

		// terminate the session
		spark.stop();
	}

	private static MatrixEntry createNewGraph(Tuple3<Long, Long, Double> f, Long bottleneck, float reductionFactor) {
		
		if(f._1().equals(bottleneck) || f._2().equals(bottleneck)) {
			return new MatrixEntry(f._1(), f._2(), f._3()*reductionFactor);
		}
		return new MatrixEntry(f._1(), f._2(), f._3());
	}

	public static JavaPairRDD<Long, Tuple3<Long, Long, Double>> prim(JavaPairRDD<Long, Vector> matrix, int numberOfAirports, SparkSession spark) throws FileNotFoundException {

		List<Tuple2<Long, Tuple3<Long, Long, Double>>> l = new ArrayList<Tuple2<Long, Tuple3<Long, Long, Double>>>(numberOfAirports);
		List<Boolean> alreadyCalc = new ArrayList<Boolean>(numberOfAirports);
		Random rand = new Random();
		int node = rand.nextInt((int) numberOfAirports);
		for (int i = 0; i < numberOfAirports; i++) {
			Tuple3<Long, Long, Double> aux = new Tuple3<Long, Long, Double>((long) i, Long.MAX_VALUE, Double.MAX_VALUE); 
			Tuple3<Long, Long, Double> auxnode = new Tuple3<Long, Long, Double>((long) node,(long) node,0.0); 
			
			if(i==node) {
				l.add(i, new Tuple2<Long, Tuple3<Long, Long, Double>>((long)i, auxnode));
				alreadyCalc.add(true);
			}else {
				l.add(i, new Tuple2<Long, Tuple3<Long, Long, Double>>((long)i, aux));
				alreadyCalc.add(false);
			}
		}
		@SuppressWarnings("resource")
		JavaPairRDD<Long, Tuple3<Long, Long, Double>> d =  new JavaSparkContext(spark.sparkContext()).parallelize(l).mapToPair(r -> r);

		for (int i = 0; i < numberOfAirports; i++) {						
			System.out.println("-------------------  process node: " + node);	
			
			JavaPairRDD<Long, Tuple2<Tuple3<Long, Long, Double>, Vector>> dmatrix = d.repartition(matrix.getNumPartitions()).join(matrix);
			for(Tuple2<Long, Tuple2<Tuple3<Long, Long, Double>, Vector>> x: dmatrix.collect())
				System.out.println(x);

			int aux = node;
			alreadyCalc.set(node, true);
			d = dmatrix.mapToPair(e -> new Tuple2<Long, Tuple3<Long, Long, Double>> (e._1, updateD(aux, e._2,alreadyCalc.get((int)(long)e._1))));
			Tuple2<Long, Tuple3<Long, Long, Double>> calcMin = d.reduce((x,y)->(getMin(x,y,alreadyCalc.get((int)(long)x._1),alreadyCalc.get((int)(long)y._1))));
			node=(int)(long)calcMin._1;

		}

		return d;

	}
	static Tuple2<Long, Tuple3<Long, Long, Double>> getMin(Tuple2<Long, Tuple3<Long, Long, Double>>x, Tuple2<Long, Tuple3<Long, Long, Double>> y,boolean xAlready, boolean yAlready){
		Tuple3<Long, Long, Double> aux = new Tuple3<Long,Long,Double>(x._2._1(),x._2._2(),Double.MAX_VALUE);
		if(xAlready && yAlready)
			return new Tuple2<Long,Tuple3<Long,Long,Double>>(x._1,aux);
		else if(xAlready)
			return y;
		else if(yAlready)
			return x;

		return y._2._3() < x._2._3() ? y : x;
	}

	private static  Tuple3<Long, Long, Double> updateD(int node, Tuple2<Tuple3<Long, Long, Double>, Vector> entry,boolean xAlready) {
		SparseVector sv  = entry._2.toSparse();
		double value = sv.apply(node);
		if (value == 0 || xAlready) { // node entry for node
			return entry._1;
		}
		Tuple3<Long, Long, Double> value2 = new Tuple3<Long, Long, Double>(entry._1._1(),(long)node, value);
		return entry._1._3() < value ? entry._1 : value2;
	}
}